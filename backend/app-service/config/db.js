/**
*
* Database configuration
*
**/

var mongoose = require('mongoose');
var db_uri = process.env.MONGOLAB_URI || 'mongodb://localhost:27017/chelaton_trivia';

mongoose.connect(db_uri);

mongoose.connection.on('open',function(){
	console.log('\033[36m \t[OK] mongoose CONNECTED to '+db_uri+' \033[39m\n');
});
mongoose.connection.on('error',function(){
	console.log('\033[31m \t[ERROR] Impossible to connect to '+db_uri+' \033[39m');
});