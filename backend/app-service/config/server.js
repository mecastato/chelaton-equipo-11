/**
 *
 * LLaves de  comunicación entre servidores
 *
 */

module.exports = {

	server_access_key : process.env.CHELATON_TRIVIA_SERVER_KEY,

	KEY_GCM : process.env.CHELATON_GCM_API_KEY

};