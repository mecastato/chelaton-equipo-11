/**
 *
 * App router
 *
 */

var express = require('express'),
		conf = require('./config/server'),
		auth = require('./lib/ss_auth')( conf.server_access_key );
		router = express.Router();

// Controllers
var questions = require('./controllers/questions_controller'),
		trivias = require('./controllers/trivias_controller'),
		devices = require('./controllers/devices_controller');


/* GET home page. */
router.get('/', function(req, res) {
	return res.send({status:'ready'});
});


/*----------  Devices (device registration)  ----------*/

router.post('/devices', devices.add);



/*----------  Questions  ----------*/

// Add question
// router.post('/questions', auth.required, questions.add);
router.post('/questions', questions.add);

// Find questions (by name)
// router.get('/questions', auth.required, questions.find);
router.get('/questions/match/:token', questions.find);

// Feed questions
// router.get('/questions', auth.required, questions.list);
router.get('/questions', questions.list);

// Delete questions
// router.delete('/questions', auth.required, questions.delete);
router.delete('/questions', questions.delete);


/*----------  Trivias  ----------*/

// Find trivia
router.get('/trivias/:trivia_id',trivias.find);

// Crear trivia
// router.post('/trivias', auth.required, trivias.add);
router.post('/trivias', trivias.add);

// Feed trivias
// router.get('/trivias', auth.required, trivias.list);
router.get('/trivias', trivias.list);

// Delete trivias
// router.delete('/trivias', auth.required, trivias.delete);
router.delete('/trivias', trivias.delete);

// Verify question results
router.post('/trivias/verify/:trivia_id', trivias.verify);

/**
 *
 * Consumida por la app.
 * La función más importante
 *
 */
// router.get('/trivia',trivias.current);
router.get('/trivia',trivias.test);


module.exports = router;