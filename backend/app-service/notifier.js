/**
 *
 * Push notification/email service
 * Created by @dantaex in 15 mins
 */
var request = require('request'),
		GCM_KEY = require('./config/server').KEY_GCM,
		Device = require('./models/devices');

module.exports = {


	/**
	 *
	 * Send a push notification to Google's GCM
	 * @param {Object} data : 
	 */
	__push : function(data){

		var done = null;
		console.log('Accesing GCM with:',GCM_KEY);
		request(
			{
			    method: 'POST',
			    uri:'https://android.googleapis.com/gcm/send',
			    headers : {
			        'Content-Type': 'application/json',
			        'Authorization': "key=" + GCM_KEY
			    },
			    port : '5228',
			    body : JSON.stringify(data) //serialized string JSON
			},
			function(remote_err,remote_response,body){
			  console.log('GOOGLE response:',body);
			  if( typeof done == 'function'){
			    done(remote_err,remote_response);
			  }
			}
		);

		return { 
			then: function( callback ){ done = callback; }
		};
	},


	/**
	 *
	 * Envía una trivia a las apps
	 *
	 */
	_pushTrivia : function(trivia){

		//please replace this with topics
		var recipient_ids = [];
		Device.find({},function(err,devices){

			for (var i = 0; i < devices.length; i++) {
				recipient_ids.push( devices[i].registration_id );
			}
	
			module.exports.__push({
				"registration_ids" : recipient_ids,
				// "to" : "/topics/food", //topics not working on ionic for now
				"data" : {
					"body" : "Gánate unas chelas con la Trivia Now!",
					"title" : "Trivia Now",
					"icon" : "icon" 
				}
			});
			
		});

	}

};