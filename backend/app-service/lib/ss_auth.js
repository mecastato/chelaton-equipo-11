/**
 *
 * Server auth
 * Token is calculated as :   sha256 ( private_key + url + body )
 *
 */

var sha256 = require('./sha256');

module.exports = function(private_key){
	
	return {
	
		required : function(req,res,next){
			var local_token = sha256.hash( private_key + req.url + JSON.stringify(req.body) );
			if( local_token === req.headers.auth ){
				next();
			} else {
				return res.status(401).send();
			}
		}
	
	};

};