/**
 *
 * Devices model
 *
 */
var mongoose = require('mongoose');

var DeviceSchema = mongoose.Schema({

	//GCM registration ID
	registration_id : {type: String, required: true, unique : true },

	//creation timestamp
	created : { type:Date, default:Date.now, select : false }

});

module.exports = mongoose.model('Device',DeviceSchema);