/**
 *
 * Trivias model
 * Las trivias son grupos de preguntas
 *
 */
var mongoose = require('mongoose');

var TriviaSchema = mongoose.Schema({

	//Opción múltiple
	questions : [
		{ type: mongoose.Schema.ObjectId, ref : 'Question' }
	],

	//When to publish it 
	publish_on : { type:Date, required: true },

	//creation timestamp
	created : { type:Date, default:Date.now, select : false }

});

/**
 * Validar mínimo número de preguntas (1)
 */
TriviaSchema.pre('save',function(next){	
	return (this.questions.length)? next() : next(new Error('Se necesita al menos una pregunta'));
});

module.exports = mongoose.model('Trivia', TriviaSchema);