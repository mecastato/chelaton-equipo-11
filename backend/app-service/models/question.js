/**
 *
 * Questions model

	{
		_id : '1234567876543234567876'
		name : "¿Quién anotó el primer Gol?",
		answers: [
			"Luis García",
			"Manuel Mijares",
			"Pelé"
		],
		time_limit : 30
	}

 *
 */
var mongoose = require('mongoose');

var QuestionSchema = mongoose.Schema({

	// El nombre de la trivia
	name : {type:String, required:true},

	//Opción múltiple
	answers : [
		{type:String}
	],

	//Respuesta correcta
	right_answer : {type:Number,required:true,select:false},

	//Time limit in seconds
	time_limit : {type:Number, default:40},

	//creation timestamp
	created : { type:Date, default:Date.now, select : false }

});

module.exports = mongoose.model('Question', QuestionSchema);