/**
 *
 * Questions controller
 * Made in 11 mins
 */

var Question = require('../models/question');

module.exports = {

	/**
	 * 
	 * POST
	 * Create a question
	 * 
	 */
	add : function(req,res){
		var question = new Question(req.body);
		question.save(function(err,doc){
			if(err) res.status(500).send(err);
			else res.send(doc);
		});
	},


	/**
	 *
	 * GET
	 * Find questions by name
	 *
	 */
	find : function(req,res){
		Question.find({ name : { $regex: req.params.token, $options: "i" } })
			.limit(50)
			.exec(function(err,docs){
				if(err) res.status(500).send();
				else res.send(docs);
			});
	},


	/**
	 *
	 * GET
	 * List questions
	 *
	 */
	list : function(req,res){
	 	var query = (req.params.before && req.params.before != 'null')? { created: { $lt: req.params.before } } : {};
	 	Question.find(query)
	 		.limit(50)
			.sort({ created: -1 })
			.exec(function(err,docs){
				if(err) res.status(500).send();
				else res.send(docs);
			});	 		

	},	


	/**
	 *
	 * DELETE
	 * Delete questions
	 *
	 */
	delete : function(req,res){
		Question.findOneAndRemove({_id: { $in:[req.params.question_id]} },function(err,doc){
			if(err) res.status(400).send();
			else res.send('done');
		});
	}


};