/**
 *
 * Trivias controller
 * Trivias are groups of questions
 */

var Trivia = require('../models/trivia'),
		mongoose = require('mongoose');

module.exports = {

	/**
	 *
	 * POST
	 * Create a trivia
	 *
	 */
	add : function(req,res){
		var trivia = new Trivia(req.body);
		trivia.save(function(err,doc){
			if(err) res.status(500).send(err);
			else res.send(doc);

			// Idealmente esto debería estar en un worker para que se enviara 
			// la notificación utilizando la propiedad publish_on
			// para publicarla en le momento adecuado, pero fuck it es un hackatón
			var notifier = require('../notifier');
			notifier._pushTrivia(doc);

		});
	},


	/**
	 *
	 * GET
	 * Find a trivia
	 *
	 */
	find : function(req,res){
		Trivia.findOne({_id: { $in: [req.params.trivia_id] }})
			.populate('questions')
			.exec(function(err,doc){
				if(err) res.status(500).send(err);
				else res.send(doc);
			});
	},


	/**
	 *
	 * GET
	 * List trivias
	 *
	 */
	list : function(req,res){
	 	var query = (req.params.before && req.params.before != 'null')? { created: { $lt: req.params.before } } : {};
	 	Trivia.find(query)
	 		.limit(50)
	 		.populate('questions')
			.sort({ created: -1 })
			.exec(function(err,docs){
				if(err) res.status(500).send();
				else res.send(docs);
			});	 		
	},


	/**
	 *
	 * DELETE
	 * Delete trivias
	 *
	 */
	delete : function(req,res){
		Trivia.findOneAndRemove({_id: { $in:[req.params.trivia_id]} },function(err,doc){
			if(err) res.status(400).send();
			else res.send('done');
		});
	},

	/**
	 *
	 * GET
	 * Get current trivia (the last one in the last hour)
	 *
	 */
	current : function(req,res){
		Trivia.findOne({
				created:{$gt:new Date(Date.now() - 60*60 * 1000)}
			})
			.populate('questions')
			.exec(function(err,doc){
				if(err) res.status(400).send();
				else res.send(doc);
			});
	},


	/**
	 *
	 * POST
	 * Verify answers
	 *
	 * receives req.body.questions as :
	 * {
	 *   answers : [ 0, 1, 2 ]
	 * }
	 *
	 */
	verify : function(req,res){
		Trivia.findOne({ _id : { $in : [req.params.trivia_id] } })
			.populate('questions','right_answer')
			.exec(function(err,trivia){
				
				var l = trivia.questions.length;
				for (var i = 0; i < trivia.questions.length; i++) {
					if( req.body.answers[i] == trivia.questions[i].right_answer ){
						l--;
					}
				}
				if(!l){
					res.send({status:'winner',code:Math.floor( Math.random() * 1000 )});
				} else {
					res.send({status:'looser'});
				}

			});
	},


	/**
	 *
	 * TESTING METHOD (hackaton :D)
	 *
	 */
	test : function(req,res){
		Trivia.findOne({})
			.populate('questions')
			.exec(function(err,doc){
				if(err) res.status(400).send();
				else res.send(doc);
			});
	}



};