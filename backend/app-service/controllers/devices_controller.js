/**
 *
 * Devices controller
 *
 */

var Device = require('../models/devices');

module.exports = {

	/**
	 *
	 * POST
	 * register a device
	 *
	 */
	add : function(req,res){


		Device.findOne({registration_id:req.body.registration_id},function(err,old_device){
			if(old_device){
				//Device exists
				res.send(old_device);
			} else if(!old_device){
				//Create device
				var appclient = new Device(req.body);
				appclient.save(function(err){
					console.log('err storing device', err);
					if(err) res.status(400).send(err.message);
					else res.send(appclient);
				});
			} else {
				//Server Error
				res.status(500).send('Server Error :(');
			}

		});
		
	},

};