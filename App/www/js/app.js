// Ionic Starter App

angular.module('starter', [
  'ionic', 
  'ngCordova',
  'starter.run', 
  'starter.trivia', 
  'starter.scores', 
  'starter.score', 
  'starter.store', 
  'starter.services',
  'starter.logros'
])

.run(function($ionicPlatform, $ionicSlideBoxDelegate) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

//Platform config
.config(function($ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.tabs.style('standard');
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.scores', {
    url: '/scores',
    views: {
      'tab-scores': {
        templateUrl: 'templates/tab-scores.html',
        controller: 'ScoresCtrl'
      }
    }
  })

  .state('tab.score', {
    url: '/scores/:id',
    views: {
      'tab-scores': {
        templateUrl: 'templates/tab-score.html',
        controller: 'ScoreCtrl'
      }
    }
  })

  .state('tab.logros', {
    url: '/logros',
    views: {
      'tab-logros': {
          templateUrl: 'templates/tab-logros.html',
          controller: 'LogrosCtrl'
        }
    }
  })

  .state('tab.trivia', {
    url: '/trivia',
    views: {
      'tab-trivia': {
          templateUrl: 'templates/tab-trivia.html',
          controller: 'TriviaCtrl'
        }
    }
  })

  .state('tab.store', {
    url: '/store',
    views: {
      'tab-store': {
          templateUrl: 'templates/tab-store.html',
          controller: 'StoreCtrl'
        }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/scores');

});
