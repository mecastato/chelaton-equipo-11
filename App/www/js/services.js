angular.module('starter.services', [])

.factory('TriviaService', function($http) {
  var trivia = [];

  return {

    getTrivia: function() {

      return $http.get("https://chelaton-app-service.herokuapp.com/trivia").then(function(response) {
        return response.data.questions;
      });
    }
  }
})

.factory('ScoresService', function($http) {
	var longUrl = "http://www.resultados-futbol.com/scripts/api/api.php?tz=America/Mexico_City&format=json&req=matchsday&key=9a61df06f85500f7ebc02b5a8c5141c7&date=" + new Date(new Date().getTime() /*+ 86400000*/).toISOString().slice(0,10) + "&country=MX";
	return {

		getTodayMatches: function() {
			return $http.get(longUrl)
				.then(function(response) {
					return response.data.matches
			});
		}
	}
})

.factory('CurrentService', function($http) {
	var longUrl = "http://www.resultados-futbol.com/scripts/api/api.php?tz=America/Mexico_City&format=json&req=livescore&key=9a61df06f85500f7ebc02b5a8c5141c7" + "&country=MX";
	return {

		getCurrentMatches: function() {
			return $http.get(longUrl)
				.then(function(response) {
					return response.data.matches
			});
		}
	}
})

.factory('MatchService', function($http) {
	return {

		getMatch: function(matchId) {
			var longUrl = "http://www.resultados-futbol.com/scripts/api/api.php?tz=America/Mexico_City&format=json&req=match&key=9a61df06f85500f7ebc02b5a8c5141c7&id=184216&id=" + matchId;
			return $http.get(longUrl)
				.then(function(response) {
					return response.data;
			});
		}
	}
})
