angular.module('starter.scores',[])
.controller('ScoresCtrl', function($scope, ScoresService, CurrentService, $state) {
  $scope.matches = ScoresService.getTodayMatches()
  	.then(function(matches) {
  		$scope.matches = matches;
  	});

  $scope.currents = CurrentService.getCurrentMatches()
  	.then(function(currents){
  		$scope.currents = currents; 
  		console.log(currents);
  	});

  $scope.goToMatch = function(idParam) {
  	console.log(idParam);
  	$state.go('tab.score', {id: idParam});
  }

});