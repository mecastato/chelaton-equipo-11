/**
 *
 * Trivia controller
 *
 */

angular.module('starter.trivia',['starter.services'])
.controller('TriviaCtrl', function($scope, TriviaService, $ionicSlideBoxDelegate, $timeout) {
  $scope.trivias = TriviaService.getTrivia()
    .then(function(trivias){
      $scope.trivias = trivias;
      $ionicSlideBoxDelegate.enableSlide(false);
      $ionicSlideBoxDelegate.update();
      $scope.counter = trivias[0].time_limit;
      startCounter();
    });

  $scope.nextTrivia = function() {
    if ($ionicSlideBoxDelegate.currentIndex() < 2)
      $scope.counter = $scope.trivias[$ionicSlideBoxDelegate.currentIndex() + 1].time_limit;
    $ionicSlideBoxDelegate.next();
  };

  function startCounter() {
    var interval = setInterval(function() {
      if ($scope.counter != 0) {
        $scope.counter--;
        $scope.$apply();
      } else {
        clearInterval(interval);
        $scope.nextTrivia();
        startCounter();
      }
    }, 1000);
  }
});
