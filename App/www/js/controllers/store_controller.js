angular.module('starter.store',[])
.controller('StoreCtrl', function($scope,$rootScope,$timeout, ScoresService,$location,$ionicModal,$ionicScrollDelegate,$cordovaToast) {


	$scope.current_beer = {};


	$scope.checkout = function(){
		$scope.modal.hide();
		$ionicScrollDelegate.$getByHandle('ionv_store').scrollTop();
	};

	$scope.blocker = false;

	$scope.buy = function(){
		//enviar compra a MOdelo
		// Ellos no tienen API, por lo que simulamos...
		$scope.blocker = true;
		$timeout(function(){
			$scope.blocker = false;
			//limpiar carrito
			$rootScope.cart_total = 0;
			$rootScope.cart.splice(0,$rootScope.cart.length);
			$cordovaToast.showShortCenter('Tu pedido está en camino');
		},750);

	};

	$scope.more = function(pack){
		pack.chosen_times++;
		$rootScope.cart_total += pack.price;
		if( $rootScope.cart.indexOf(pack) === -1 ){
			$rootScope.cart.push(pack);
		}

	};

	$scope.less = function(pack){
		if(pack.chosen_times === 0){
			return;
		} else {
			pack.chosen_times--;
			$rootScope.cart_total -= pack.price;
			var index = $rootScope.cart.indexOf(pack);
			$rootScope.cart.splice(index,1);
		}
	};


	//UI stuff
	$ionicModal.fromTemplateUrl('templates/store_modal.html', {
	  scope: $scope,
	  animation: 'slide-in-up'
	}).then(function(modal) {
		console.log('TOO GOOD FOR A SINNER LIKE ME');
	  $scope.modal = modal;
	});

	$scope.openModal = function(beer) {
		$scope.current_beer = beer;
	  $scope.modal.show();
	};
	$scope.closeModal = function() {
	  $scope.modal.hide();
	};
	//Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
	  $scope.modal.remove();
	});
	

});
