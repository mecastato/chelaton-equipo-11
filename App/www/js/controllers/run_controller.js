/**
 *
 * Controlador de configuración de Ionic
 *
 */

angular.module('starter.run',[])
.controller('DashCtrl', function($scope, $rootScope, $cordovaPush, $cordovaMedia, $cordovaToast, $cordovaDialogs, $ionicPlatform, $http, $ionicPopup) {
    
    $rootScope.cart = [];
    $rootScope.cart_total = 0;

    $scope.notifications = [];
    // call to register automatically upon device ready
    ionic.Platform.ready(function() {
        console.log("Dispositivo listo");
        $scope.register();
    });
    // Register
    $scope.register = function() {
            var config = null;
            console.log("Entro a register");
            if (ionic.Platform.isAndroid()) {
                config = {
                    "senderID": "641617501407",                   
                    "ecb": "angular.element(document.body).injector().get('$cordovaPush').onNotification"
                };
            } else if (ionic.Platform.isIOS()) {
                config = {
                    "badge": "true",
                    "sound": "true",
                    "alert": "true"
                }
            }
            $cordovaPush.register(config).then(function(result) {
                console.log("Register success " + result);
                $cordovaToast.showShortCenter('Registered for push notifications');
                $scope.registerDisabled = true;
                // ** NOTE: Android regid result comes back in the pushNotificationReceived, only iOS returned here
                if (ionic.Platform.isIOS()) {
                    console.log("ENTRO A IOS");
                    $scope.regId = result;
                    storeDeviceToken("iOS");
                }
            }, function(err) {
                console.log("Register error " + err)
            });
        } //End scope.register
        // Notification Received for both Android and IOS
    $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
        console.log("onNotificationReceived: " + JSON.stringify([notification]));
        if (ionic.Platform.isAndroid()) {
            handleAndroid(notification);
        } else if (ionic.Platform.isIOS()) {
            handleIOS(notification);
            $scope.$apply(function() {
                $scope.notifications.push(JSON.stringify(notification.alert));
            })
        }
    });
    //Android Notification Received Handler
    function handleAndroid(notification) {
        console.log("In foreground " + notification.foreground  + " Coldstart " + notification.coldstart);
        switch (notification.event) {
            case 'registered':
                if (notification.regid.length > 0) {
                    console.log('registration ID = ' + notification.regid);
                    $scope.regId = notification.regid;
                    $scope.registrationID = notification.regid;
                    storeDeviceToken("Android");
                }
                break;
            case 'message':
                //AQUI LLEGA LA PUSH
                //$cordovaDialogs.alert(notification.message, "Push Notification Received");
                $scope.$apply(function () {
                    $scope.notifications.push(JSON.stringify(notification.message));
                })
                if (notification.coldstart){
                    console.log("COOLDSTART");
                    $state.go("tab.trivia");                             
                    window.open(encodeURI('#/tab/trivia'), '_system');                    
                }
                break;
            case 'error':
                $console.log(notification.msg, "Push notification error event");
                break;
            default:
                console.log('An unknown GCM event has occurred');
                break;
        }
    } //END HANDLEANDROID
    // IOS Notification Received Handler
    function handleIOS(notification) {
        // The app was already open but we'll still show the alert and sound the tone received this way. If you didn't check
        // for foreground here it would make a sound twice, once when received in background and upon opening it from clicking
        // the notification when this code runs (weird).
        if (notification.foreground == "1") {
            if (notification.body && notification.messageFrom) {
                $cordovaDialogs.alert(notification.body, notification.messageFrom);
            } else $cordovaDialogs.alert(notification.alert, "Push Notification Received");
            if (notification.badge) {
                $cordovaPush.setBadgeNumber(notification.badge).then(function(result) {
                    console.log("Set badge success " + result)
                }, function(err) {
                    console.log("Set badge error " + err)
                });
            }
        }
        // Otherwise it was received in the background and reopened from the push notification. Badge is automatically cleared
        // in this case. You probably wouldn't be displaying anything at this point, this is here to show that you can process
        // the data in this situation.
        else {
            if (notification.body && notification.messageFrom) {
                $cordovaDialogs.alert(notification.body, "(RECEIVED WHEN APP IN BACKGROUND) " + notification.messageFrom);
            } else $cordovaDialogs.alert(notification.alert, "(RECEIVED WHEN APP IN BACKGROUND) Push Notification Received");
        }
    }
    // Stores the device token in server
    function storeDeviceToken(type) {
        console.log("Entro a storeDeviceToken");
        $http.post('http://chelaton-app-service.herokuapp.com/devices', {
            "registration_id": $scope.regId
        }).then(function successCallback(response, status) {
            // this callback will be called asynchronously
            // when the response is available
            console.log("Token storing successfully: " + response + ", Status: " + status);
        }, function errorCallback(response, status) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log("Error storing device token." + response);
            console.log(response);
        });
    }



    // Hard data (for now)
    //No opportunities for scrapping now.
    $scope.beers = [
        {
            "name" : "Bud Light",
            "packs" : [
                {
                    "_id" : 1,
                    "chosen_times":0,
                    "name" : "Bud Light",
                    "price" : 293.00,
                    "desc" : "Combo: 24 Pack - 355 ml + 6 Pack - 740 ml",
                    "media" : "budlight.png"
                },
                {
                    "_id" : 5,
                    "chosen_times":0,
                    "name" : "Bud Light",
                    "price" : 260.00,
                    "desc" : "30 Pack - Bote - 355 ml - No Retornable",
                    "media" : "budlight.png"
                },
                {
                    "_id" : 6,
                    "chosen_times":0,
                    "name" : "Bud Light",
                    "price" : 215.00,
                    "desc" : "24 Pack - Bote - 355 ml - No Retornable",
                    "media" : "budlight.png"
                },
                {
                    "_id" : 7,
                    "chosen_times":0,
                    "name" : "Bud Light",
                    "price" : 120.00,
                    "desc" : "12 Pack - Bote - 355 ml - No Retornable",
                    "media" : "budlight.png"
                }
            ]
        },
        {
            "name" : "Corona Extra",
            "packs" : [
                {
                    "_id" : 2,
                    "chosen_times":0,
                    "name" : "Corona Extra",
                    "price" : 120.00,
                    "desc" : "12 Pack - Bote - 355 ml - No Retornable",
                    "media" : "corona_extra.png"
                },
                {
                    "_id" : 8,
                    "chosen_times":0,
                    "name" : "Corona Extra",
                    "price" : 230.00,
                    "desc" : "12 Pack - Bote - 355 ml - No Retornable",
                    "media" : "corona_extra.png"
                }
            ]
        },
        {
            "name" : "Modelo Especial",
            "packs" : [
                {
                    "_id" : 4,
                    "chosen_times":0,
                    "name" : "Modelo Especial",
                    "price" : 155.00,
                    "desc" : "12 Pack - Bote - 355 ml - No Retornable",
                    "media" : "modelo_especial.png"
                },
                {
                    "_id" : 10,
                    "chosen_times":0,
                    "name" : "Modelo Especial",
                    "price" : 300.00,
                    "desc" : "24 Pack - Bote - 355 ml - No Retornable",
                    "media" : "modelo_especial.png"
                }           
            ]
        },
        {
            "name": "Corona Light",
            "packs" : [
                {
                    "_id" : 9,
                    "chosen_times":0,
                    "name" : "Corona Light",
                    "price" : 230.00,
                    "desc" : "24 Pack - Bote - 355 ml - No Retornable",
                    "media" : "corona_light.png"
                }
            ]
        },
        {
            "name" : "Michelob Ultra",
            "packs" : [
                {
                    "_id" : 3,
                    "chosen_times":0,
                    "name" : "Michelob Ultra",
                    "price" : 180.00,
                    "desc" : "12 Pack - Botella Importada - 12 oz - No Retornable",
                    "media" : "michelob.png"
                },
                {
                    "_id" : 11,
                    "chosen_times":0,
                    "name" : "Michelob Ultra",
                    "price" : 355.00,
                    "desc" : "24 Pack - Botella Importada - 12 oz - No Retornable",
                    "media" : "michelob.png"
                }
            ]
        },
        {
            "name" : "Stella Artois",
            "packs" : [
                {
                    "_id" : 11.5,
                    "chosen_times":0,
                    "name" : "Stella Artois",
                    "price" : 450.00,
                    "desc" : "24 Pack - Botella Importada - 330 ml - No Retornable",
                    "media" : "stella_artois.png"
                }
            ]
        },
        {
            "name" : "Barrilito",
            "packs" : [
                {
                    "_id" : 15,
                    "chosen_times":0,
                    "name" : "Barrilito",
                    "price" : 245.00,
                    "desc" : "24 Pack - Botella - 325 ml - No Retornable",
                    "media" : "barrilito.png"
                }
            ]
        },
        {
            "name" : "Bocanegra Dunkel",
            "packs" : [
                {
                    "_id" : 16,
                    "chosen_times":0,
                    "name" : "Bocanegra Dunkel",
                    "price" : 256.00,
                    "desc" : "8 Pack - Botella - 355 ml - No Retornable",
                    "media" : "bocanegra.png"
                }
            ]
        },
        {
            "name" : "Bocanegra Pilsner",
            "packs" : [
                {
                    "_id" : 17,
                    "chosen_times":0,
                    "name" : "Bocanegra Pilsner",
                    "price" : 256.00,
                    "desc" : "8 Pack - Botella - 355 ml - No Retornable",
                    "media" : "bocanegra2.png"
                }
            ]
        },
        {
            "name" : "Ritas",
            "packs" : [
                {
                    "_id" : 12,
                    "chosen_times":0,
                    "name" : "Bud Light - StrawBerRita",
                    "price" : 270.00,
                    "desc" : "24 Pack - Bote Importado - 8 oz - No Retornable",
                    "media" : "strawberrita.png"
                },
                {
                    "_id" : 13,
                    "chosen_times":0,
                    "name" : "Bud Light - LimeARita",
                    "price" : 270.00,
                    "desc" : "24 Pack - Bote Importado - 8 oz - No Retornable",
                    "media" : "limearrita.png"
                },
                {
                    "_id" : 14,
                    "chosen_times":0,
                    "name" : "Bud Light - RazBerRita",
                    "price" : 270.00,
                    "desc" : "24 Pack - Bote Importado - 8 oz - No Retornable",
                    "media" : "razberrita.png"
                }
            ]
        }   
    ];

});