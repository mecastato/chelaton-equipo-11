angular.module('starter.score',[])
.controller('ScoreCtrl', function($scope, $stateParams, MatchService) {

	

	$scope.order_item = "minute";
	$scope.order_reverse = false;

	$scope.match = MatchService.getMatch($stateParams.id)
  	.then(function(match){
  		$scope.match = match; 
  		console.log(match);
  		$scope.events = $scope.match.events.cards.concat($scope.match.events.changes.concat($scope.match.events.goals));
  	});

});
